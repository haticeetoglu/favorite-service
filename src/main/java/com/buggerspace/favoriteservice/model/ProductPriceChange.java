package com.buggerspace.favoriteservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductPriceChange {

    private Integer productId;
    private Integer oldPrice;
    private Integer newPrice;
    
}
