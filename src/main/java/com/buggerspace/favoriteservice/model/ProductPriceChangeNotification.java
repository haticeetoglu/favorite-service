package com.buggerspace.favoriteservice.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ProductPriceChangeNotification {
    private Integer productId;
    private Integer oldPrice;
    private Integer newPrice;
    private List<Integer> userIds = new ArrayList<>();
}
