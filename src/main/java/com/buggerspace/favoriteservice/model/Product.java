package com.buggerspace.favoriteservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Product {
    private Integer productId;
    private String productName;
    private Integer unitPrice;
    private Integer unitInStock;
    private Integer categoryId;
}
