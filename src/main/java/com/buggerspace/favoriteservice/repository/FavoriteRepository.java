package com.buggerspace.favoriteservice.repository;

import com.buggerspace.favoriteservice.entity.FavoriteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FavoriteRepository extends CrudRepository<FavoriteEntity, Integer> {

    boolean existsByUserIdAndProductId(Integer userId, Integer productId);

    Optional<FavoriteEntity> findByUserIdAndProductId(Integer userId, Integer productId);

    List<FavoriteEntity> findByUserId(Integer userId);

    List<FavoriteEntity> findByProductId(Integer productId);
}
