package com.buggerspace.favoriteservice.service;

import com.buggerspace.favoriteservice.entity.FavoriteEntity;
import com.buggerspace.favoriteservice.repository.FavoriteRepository;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class FavoriteService {

    public final FavoriteRepository favoriteRepository;

    public List<FavoriteEntity> getFavoritesOfUser(Integer userId){
        return favoriteRepository.findByUserId(userId);
    }

    public List<FavoriteEntity> getUsersOfFavoriteProduct(Integer productId){ return favoriteRepository.findByProductId(productId); }

    public void addFavorite(Integer userId, Integer productId){
        if(!favoriteRepository.existsByUserIdAndProductId(userId, productId)){
            FavoriteEntity favoriteEntity = new FavoriteEntity();
            favoriteEntity.setUserId(userId);
            favoriteEntity.setProductId(productId);
            Date now = new Date();
            favoriteEntity.setCreateDate(now);
            favoriteEntity.setUpdateDate(now);
            favoriteEntity.setDeleted(false);
            favoriteRepository.save(favoriteEntity);
        }
    }

    public void removeFavorite(Integer userId, Integer productId) throws NotFoundException {
        Optional<FavoriteEntity> favoriteEntity = favoriteRepository.findByUserIdAndProductId(userId, productId);
        if(favoriteEntity.isPresent()){
            FavoriteEntity favorite = favoriteEntity.get();
            favorite.setDeleted(true);
            Date now = new Date();
            favorite.setUpdateDate(now);
            favoriteRepository.save(favorite);
        }else{
            throw new NotFoundException("This product is not in the user's favorites");
        }
    }
}
