package com.buggerspace.favoriteservice.service;

import com.buggerspace.favoriteservice.entity.FavoriteEntity;
import com.buggerspace.favoriteservice.model.ProductPriceChange;
import com.buggerspace.favoriteservice.model.ProductPriceChangeNotification;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class KafkaService {

    private final FavoriteService favoriteService;
    private final KafkaTemplate<String, ProductPriceChangeNotification> kafkaTemplate;
    private final KafkaTemplate<String, ProductPriceChange> kafkaTemplate2;

    @Value("${spring.kafka.topic.product-price-change-notification}")
    private String productPriceChangeNotificationDestination;

    @Value("${spring.kafka.topic.product-price-change}")
    private String productPriceChangeDestination;

    @KafkaListener(
            topics = "${spring.kafka.topic.product-price-change}",
            groupId = "product-price-change-group",
            clientIdPrefix = "product-price-change-group",
            containerFactory = "productPriceChangeQueueKafkaListenerContainerFactory"
    )
    public void productPriceChangeKafkaListener(ProductPriceChange productPriceChange) {
        List<FavoriteEntity> favoriteEntities = favoriteService.getUsersOfFavoriteProduct(productPriceChange.getProductId());
        List<Integer> userIds = favoriteEntities.stream().map(FavoriteEntity::getUserId).collect(Collectors.toList());

        ProductPriceChangeNotification notificationMessage = new ProductPriceChangeNotification();
        notificationMessage.setProductId(productPriceChange.getProductId());
        notificationMessage.setNewPrice(productPriceChange.getNewPrice());
        notificationMessage.setOldPrice(productPriceChange.getOldPrice());
        notificationMessage.setUserIds(userIds);

        sendMessageToNotificationService(notificationMessage);
    }


    public void sendMessageToNotificationService(ProductPriceChangeNotification productPriceChangeNotification) {
        Message<ProductPriceChangeNotification> kafkaMessage = MessageBuilder
                .withPayload(productPriceChangeNotification)
                .setHeader(KafkaHeaders.TOPIC, productPriceChangeNotificationDestination)
                .build();
        kafkaTemplate.send(kafkaMessage);
    }

    public void forTestingOnly(ProductPriceChange priceChange) {
        Message<ProductPriceChange> kafkaMessage = MessageBuilder
                .withPayload(priceChange)
                .setHeader(KafkaHeaders.TOPIC, productPriceChangeDestination)
                .build();
        kafkaTemplate2.send(kafkaMessage);
    }

}
