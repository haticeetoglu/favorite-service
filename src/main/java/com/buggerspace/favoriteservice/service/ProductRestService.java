package com.buggerspace.favoriteservice.service;

import com.buggerspace.favoriteservice.model.Product;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import java.util.List;

public interface ProductRestService {

    @POST("/api/product")
    Call<List<Product>> getProducts(@Body List<Integer> productIds);

}
