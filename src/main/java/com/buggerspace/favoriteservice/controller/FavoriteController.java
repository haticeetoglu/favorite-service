package com.buggerspace.favoriteservice.controller;

import com.buggerspace.favoriteservice.model.ProductPriceChange;
import com.buggerspace.favoriteservice.model.ProductPriceChangeNotification;
import com.buggerspace.favoriteservice.service.FavoriteService;
import com.buggerspace.favoriteservice.service.KafkaService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/favorite")
@RequiredArgsConstructor
public class FavoriteController {

    public final FavoriteService favoriteService;
    public final KafkaService kafkaService;

    @PostMapping
    public void addFavoriteForUser(@RequestParam(value = "userId") Integer userId,
                                   @RequestParam(value = "productId") Integer productId){
        favoriteService.addFavorite(userId, productId);
    }

    @GetMapping
    public ResponseEntity<?> getFavoriteProductForUser(@RequestParam(value = "userId") Integer userId){
        return ResponseEntity.status(HttpStatus.OK).body(favoriteService.getFavoritesOfUser(userId));
    }

    @DeleteMapping
    public ResponseEntity<?> removeFromFavorites(@RequestParam(value = "userId") Integer userId,
                                                 @RequestParam(value = "productId") Integer productId) {
        try{
            favoriteService.removeFavorite(userId, productId);
            return ResponseEntity.status(HttpStatus.OK).body("Product is removed from favorites.");
        }catch (NotFoundException exception){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("This product is not found in the favorites of the user.");
        }
    }

    @GetMapping("/user")
    public ResponseEntity<?> getUsersForProduct(@RequestParam(value = "productId") Integer productId){
        return ResponseEntity.status(HttpStatus.OK).body(favoriteService.getUsersOfFavoriteProduct(productId));
    }

    @PostMapping("/testkafka")
    public void testKafkaMessage(@RequestBody ProductPriceChange productPriceChange){
        kafkaService.forTestingOnly(productPriceChange);
    }
}
