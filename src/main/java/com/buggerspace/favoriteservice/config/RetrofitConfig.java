package com.buggerspace.favoriteservice.config;

import com.buggerspace.favoriteservice.service.ProductRestService;
import lombok.RequiredArgsConstructor;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

@Configuration
@RequiredArgsConstructor
public class RetrofitConfig {

    @Value("${product-service.url:#{null}}")
    private String productServiceBaseUrl;

    @Bean
    public ProductRestService productRestService() {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(10 * 60, TimeUnit.SECONDS);
        builder.connectTimeout(10 * 60, TimeUnit.SECONDS);
        OkHttpClient client = builder.build();
        return new Retrofit.Builder()
                .baseUrl(productServiceBaseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(ProductRestService.class);
    }
}
