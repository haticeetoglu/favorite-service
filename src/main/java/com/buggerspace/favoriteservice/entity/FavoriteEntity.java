package com.buggerspace.favoriteservice.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Where(clause = "deleted=false")
@Entity
@Table(name = "favorite")
@Getter
@Setter
public class FavoriteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public Integer userId;
    public Integer productId;
    public Date createDate;
    public Date updateDate;
    public Boolean deleted;
}
